package org.jmendozas.appmockito.ejemplos.repositories;

import org.jmendozas.appmockito.ejemplos.Datos;
import org.jmendozas.appmockito.ejemplos.models.Examen;
import org.jmendozas.appmockito.ejemplos.services.ExamenService;
import org.jmendozas.appmockito.ejemplos.services.ExamenServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

//OPTION 2
@ExtendWith(MockitoExtension.class)
class ExamenRepositoryImplTest {


    //mocks
    @Mock
    PreguntaRepositoryImpl preguntaRepository;
    @Mock
    ExamenRepositoryImpl repository;
    //fin mocks

    @InjectMocks
    ExamenServiceImpl examenService;


    @Captor
    ArgumentCaptor<Long> captor;


    @BeforeEach
    void setUp() {
        //OPTION 1 :habilitamos el uso de naotaciones para esta clase
//        MockitoAnnotations.openMocks(this);
        //FIN OPTION 1
//        repository = mock(ExamenRepository.class);
//        preguntaRepository = mock(PreguntaRepository.class);
//        examenService = new ExamenServiceImpl(repository,preguntaRepository);
    }


    @Test
    void findExamenPorNombre() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        Optional<Examen> examen = examenService.findExamenPorNombre("Matematicas");
        assertTrue(examen.isPresent());
        assertEquals(5L, examen.orElseThrow().getId());
        assertEquals("Matematicas", examen.get().getNombre());
    }


    @Test
    void findExamenPorNombreListaVacia() {
        List<Examen> datos = Collections.emptyList();
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        Optional<Examen> examen = examenService.findExamenPorNombre("Matematicas");
        assertTrue(examen.isPresent());
        assertEquals(5L, examen.orElseThrow().getId());
        assertEquals("Matematicas", examen.get().getNombre());
    }
    /*
     * verify , nos permite verificar si un metodo se ejecuto
     * */

    @Test
    void testPreguntasExamen() {
        //simulando que devuelva algo
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        Examen examen = examenService.findExamenPorNombreConPreguntas("Historia");
        assertEquals(5, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("integrales"));
    }


    @Test
    void testPreguntasExamenVerificar() throws InterruptedException {
        //simulando que devuelva algo
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas");
        assertEquals(5, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("integrales"));
        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(5L);
    }


    @Test
    void testNoExisteExamenVerify() throws InterruptedException {
        //simulando que devuelva algo
        //given
        when(repository.findAll()).thenReturn(Collections.emptyList());
        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        //when
        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas2");

        //then
        assertNotNull(examen);
        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(5L);
    }

    @Test
    void testGuardarExamen() {

        //Given .. son las precondiciones en nuestro entorno de prtueba
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);
        when(repository.guardar(any(Examen.class))).then(new Answer<Examen>() {
            Long secuencia = 8L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        });

        // When
        Examen examen = examenService.guardar(newExamen);

        // Then
        assertNotNull(examen.getId());
        assertEquals(8L, examen.getId());
        assertEquals("Fisica", examen.getNombre());

        verify(repository).guardar(any(Examen.class));
        verify(preguntaRepository).guardarVarias(anyList());
    }

    //43. Argument matchers
    @Test
    void textManejoException() throws InterruptedException {
        when(repository.findAll()).thenReturn(Datos.EXAMENES_ID_NULL);
        when(preguntaRepository.findPreguntasPorExamenId(isNull())).thenThrow(IllegalArgumentException.class);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            examenService.findExamenPorNombreConPreguntas("Matematicas");
        });
        assertEquals(IllegalArgumentException.class, exception.getClass());

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(isNull());
    }


    //43. Argument matchers , es una caracteristica de mockito que te permite saber si coincide el valor que se pasa por argumentors
    @Test
    void testArgumentMatcher() throws InterruptedException {
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        examenService.findExamenPorNombreConPreguntas("Matematicas");

        verify(repository).findAll();
//        verify(preguntaRepository).findPreguntasPorExamenId(argThat(arg->arg!=null && arg.equals(5L)));
        verify(preguntaRepository).findPreguntasPorExamenId(argThat(arg -> arg != null && arg >= 5L));
//        verify(preguntaRepository).findPreguntasPorExamenId(eq(5L));
    }


    @Test
    void testArgumentMatcher2() throws InterruptedException {
        when(repository.findAll()).thenReturn(Datos.EXAMENES_ID_NEGATIVOS);
        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        examenService.findExamenPorNombreConPreguntas("Matematicas");

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(argThat(new MiArgsMatchers()));
    }


    public static class MiArgsMatchers implements ArgumentMatcher<Long> {

        private Long argumento;

        @Override
        public boolean matches(Long argument) {
            this.argumento = argument;
            return argument != null && argument > 0;
        }

        @Override
        public String toString() {
            return "es para un mensaje personalizado de error " +
                    "que imprime mockito en caso de que falle el test " + argumento +
                    "  debe ser un entero positivo";
        }
    }

    @Test
    void testArgumentCaptor(){
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        examenService.findExamenPorNombreConPreguntas("Matematicas");

//        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(preguntaRepository).findPreguntasPorExamenId(captor.capture());

        assertEquals(5L,captor.getValue());

    }

    @Test
    void testDoThrow(){
        Examen examen  = Datos.EXAMEN;
        examen.setPreguntas(Datos.PREGUNTAS);
        //metodos VOID
        doThrow(IllegalArgumentException.class).when(preguntaRepository).guardarVarias(anyList());
        assertThrows(IllegalArgumentException.class , ()->{
            examenService.guardar(examen);
        });
    }


    @Test
    void testDoAnswer() throws InterruptedException {
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).then(Datos.PREGUNTAS);
        doAnswer(invocation ->{
            Long id = invocation.getArgument(0);
            return id == 5?Datos.PREGUNTAS:Collections.emptyList();
        }).when(preguntaRepository).findPreguntasPorExamenId(anyLong());
        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas");
        assertEquals(5,examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("Geometria"));
        assertEquals(5L,examen.getId());
        assertEquals("Matematicas",examen.getNombre());
        verify(preguntaRepository).findPreguntasPorExamenId(anyLong());

    }

    @Test
    void testDoAnswerGuardarExamen() {

        //Given .. son las precondiciones en nuestro entorno de prtueba
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);
        doAnswer(new Answer<Examen>() {
            Long secuencia = 8L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        }).when(repository).guardar(any(Examen.class));

        // When
        Examen examen = examenService.guardar(newExamen);

        // Then
        assertNotNull(examen.getId());
        assertEquals(8L, examen.getId());
        assertEquals("Fisica", examen.getNombre());

        verify(repository).guardar(any(Examen.class));
        verify(preguntaRepository).guardarVarias(anyList());
    }



    @Test
    void testDocCallRealMethod() throws InterruptedException {
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
   
        doCallRealMethod().when(preguntaRepository).findPreguntasPorExamenId(anyLong());
        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas");
        assertEquals(5L,examen.getId());
        assertEquals("Matematicas",examen.getNombre());
    }


    @Test
    void testSpy() throws InterruptedException {
        ExamenRepository examenRepository = spy(ExamenRepositoryImpl.class); //implementacion concreta
        PreguntaRepository preguntaRepository = spy(PreguntaRepositoryImpl.class);
        ExamenService examenService = new ExamenServiceImpl(examenRepository,preguntaRepository);

        List<String> preguntas = Arrays.asList("aritmetica");
//        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(preguntas);

        doReturn(preguntas).when(preguntaRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas");
        assertEquals(5,examen.getId());
        assertEquals("Matematicas",examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmetica"));

        verify(examenRepository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(anyLong());
    }


    //en este caso nos permitira verificar el orden en el cual se ejecuta los metodos mocks
    @Test
    void testOrdenDeInvocaciones(){
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        examenService.findExamenPorNombreConPreguntas("Matematicas");
        examenService.findExamenPorNombreConPreguntas("Lenguaje");

        InOrder inOrder = inOrder(preguntaRepository);
        inOrder.verify(preguntaRepository).findPreguntasPorExamenId(5L);
        inOrder.verify(preguntaRepository).findPreguntasPorExamenId(6L);
    }

    @Test
    void testOrdenDeInvocaciones2(){
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        examenService.findExamenPorNombreConPreguntas("Matematicas");
        examenService.findExamenPorNombreConPreguntas("Lenguaje");

        InOrder inOrder = inOrder(repository,preguntaRepository);
        inOrder.verify(repository).findAll();
        inOrder.verify(preguntaRepository).findPreguntasPorExamenId(5L);
        inOrder.verify(repository).findAll();
        inOrder.verify(preguntaRepository).findPreguntasPorExamenId(6L);
    }

    @Test
    void testNumerodeInvocaciones(){
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        examenService.findExamenPorNombreConPreguntas("Matematicas");

        verify(preguntaRepository).findPreguntasPorExamenId(5L);
        verify(preguntaRepository,times(1)).findPreguntasPorExamenId(5L);
        verify(preguntaRepository,atLeast(1)).findPreguntasPorExamenId(5L);
        verify(preguntaRepository,atLeastOnce()).findPreguntasPorExamenId(5L);
        verify(preguntaRepository,atMostOnce()).findPreguntasPorExamenId(5L);
    }

}