package org.jmendozas.appmockito.ejemplos.repositories;

import org.jmendozas.appmockito.ejemplos.Datos;
import org.jmendozas.appmockito.ejemplos.models.Examen;
import org.jmendozas.appmockito.ejemplos.services.ExamenService;
import org.jmendozas.appmockito.ejemplos.services.ExamenServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

//OPTION 2
@ExtendWith(MockitoExtension.class)
class ExamenRepositoryImplSpyTest {


    //mocks
    @Spy
    PreguntaRepositoryImpl preguntaRepository;
    @Spy
    ExamenRepositoryImpl examenRepository;
    //fin mocks

    @InjectMocks
    ExamenServiceImpl examenService;


    @Test
    void testSpy() throws InterruptedException {
        List<String> preguntas = Arrays.asList("aritmetica");
//        when(preguntaRepository.findPreguntasPorExamenId(anyLong())).thenReturn(preguntas);

        doReturn(preguntas).when(preguntaRepository).findPreguntasPorExamenId(anyLong());

        Examen examen = examenService.findExamenPorNombreConPreguntas("Matematicas");
        assertEquals(5,examen.getId());
        assertEquals("Matematicas",examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmetica"));

        verify(examenRepository).findAll();
        verify(preguntaRepository).findPreguntasPorExamenId(anyLong());
    }




}