package org.jmendozas.appmockito.ejemplos.repositories;

import java.util.List;

public interface PreguntaRepository {
    List<String> findPreguntasPorExamenId(Long id) throws InterruptedException;
    void guardarVarias(List<String> preguntas);
}
