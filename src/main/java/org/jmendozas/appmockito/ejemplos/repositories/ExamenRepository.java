package org.jmendozas.appmockito.ejemplos.repositories;

import org.jmendozas.appmockito.ejemplos.models.Examen;

import java.util.List;

public interface ExamenRepository {
    Examen guardar(Examen examen);
    List<Examen> findAll();
}
